#!/bin/bash

# URL Git-репозитория GitLab
GITLAB_REPO_URL="git@gitlab.com:svadko/svadko.git"

# Ветка, которую вы хотите отслеживать
BRANCH="main"

cd svadko_front

# Команда для запуска после обнаружения изменений
CUSTOM_COMMAND=""

# Обновить локальную копию репозитория
git pull origin main

# Сравнить локальную ветку с удаленной
if [[ $(git rev-list --left-right --count "$BRANCH...origin/$BRANCH") != "0 0" ]]; then
    echo "Изменения обнаружены в ветке $BRANCH."
    echo "Запуск задачи: $CUSTOM_COMMAND"
    $CUSTOM_COMMAND
else
    echo "Нет изменений в ветке $BRANCH."
fi
