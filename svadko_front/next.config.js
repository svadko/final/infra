/** @type {import('next').NextConfig} */
const nextConfig = {
  images: {
    domains: ["localhost", "104.248.32.114"],
    remotePatterns: [
      {
        protocol: "https",
        hostname: "**",
      },
    ],
  },
};

module.exports = nextConfig;
