'use client'
import { ReactNode } from "react";
import { Provider } from "react-redux";
import configureStore from './store';

const store = configureStore({});

const AppProvider = ({children} : {children: ReactNode}) => {
    return (
        <Provider store = {store}>
            {children}
        </Provider>
    );
}

export default AppProvider;