import styles from "../pageThemes/main.module.css";

export default function Icons({ children }: { children: React.ReactNode }) {
  return <div className={styles.Icons}>{children}</div>;
}
