import styles from "../pageThemes/main.module.css";

export default function Money_case({ money }: { money: number }) {
  return (
    <div className={styles.case}>
      <svg
        id="case"
        width="105"
        height="32"
        viewBox="0 0 105 32"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <g filter="url(#filter0_d_52_52)">
          <path
            d="M4.3125 0H100.312V24H4.3125L16.3125 12L4.3125 0Z"
            fill="#E6CD40"
          />
        </g>
        <defs>
          <filter
            id="filter0_d_52_52"
            x="0.3125"
            y="0"
            width="104"
            height="32"
            filterUnits="userSpaceOnUse"
            color-interpolation-filters="sRGB"
          >
            <feFlood flood-opacity="0" result="BackgroundImageFix" />
            <feColorMatrix
              in="SourceAlpha"
              type="matrix"
              values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
              result="hardAlpha"
            />
            <feOffset dy="4" />
            <feGaussianBlur stdDeviation="2" />
            <feComposite in2="hardAlpha" operator="out" />
            <feColorMatrix
              type="matrix"
              values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0"
            />
            <feBlend
              mode="normal"
              in2="BackgroundImageFix"
              result="effect1_dropShadow_52_52"
            />
            <feBlend
              mode="normal"
              in="SourceGraphic"
              in2="effect1_dropShadow_52_52"
              result="shape"
            />
          </filter>
        </defs>
      </svg>
    </div>
  );
}
