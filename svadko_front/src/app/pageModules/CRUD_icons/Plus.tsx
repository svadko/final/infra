import { Dispatch, SetStateAction } from "react";
import { Book } from "../Class/Book";

export default function Plus() {
  return (
    <svg
      width="32"
      height="32"
      viewBox="0 0 32 32"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M32 16C32 24.8366 24.8366 32 16 32C7.16344 32 0 24.8366 0 16C0 7.16344 7.16344 0 16 0C24.8366 0 32 7.16344 32 16Z"
        fill="#E6CD40"
      />
      <path
        d="M23.4166 16H17.4166V22H15.4166V16H9.41663V14H15.4166V8H17.4166V14H23.4166V16Z"
        fill="black"
      />
    </svg>
  );
}
