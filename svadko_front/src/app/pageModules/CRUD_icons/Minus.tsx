export default function Minus() {
  return (
    <svg
      width="33"
      height="32"
      viewBox="0 0 33 32"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M32.9375 16C32.9375 24.8366 25.7741 32 16.9375 32C8.10094 32 0.9375 24.8366 0.9375 16C0.9375 7.16344 8.10094 0 16.9375 0C25.7741 0 32.9375 7.16344 32.9375 16Z"
        fill="#E629AB"
      />
      <path d="M24.3125 17.1667H10.3125V16H24.3125V17.1667Z" fill="black" />
    </svg>
  );
}
