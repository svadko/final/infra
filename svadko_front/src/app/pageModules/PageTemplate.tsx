"use client";
import { useState } from "react";
import ProductCards from "./Cards/ProductCarts";
import styles from "./pageThemes/main.module.css";
import Banner from "./Parts/Banner";
import Buy from "./Parts/Buy";
import Left from "./Parts/Left";
import { Book } from "./Class/Book";

import {connect} from 'react-redux';
import {Dispatch, bindActionCreators} from 'redux';
import { getBooks } from "../store/actions/booksAction";
import { useEffect } from "react";

function PageTemplate(props: any) {

  const [isLoading, setIsLoading] = useState(true);

  useEffect(() =>{
    props.getBooksAction();
  },[]);

  useEffect(() => {
    if(props.books != null) setIsLoading(false);
    console.log(props);
  },[props])
  
  const [cartProducts, setCartProducts] = useState<Book[]>([]);

  const addProduct = (book: Book) => {
    setCartProducts([...cartProducts, book]);
    book.amount--;
    console.log(cartProducts);
  };


  return (
    <div>
      {/* <Banner></Banner> */}
      <div className={styles.InnerPageTemplate}>
        <Left />
        {isLoading ? (
          <div>
            Loading
          </div>
        ) : (
          <ProductCards
            books={props.books}
            addProduct={(book: Book) => (
              book.amount > 0 && setCartProducts([...cartProducts, book]),
              book.amount > 0 && book.amount--,
              book.amount > 0 && console.log(book)
            )}
            minusProduct={(book: Book) => (
              book.amount < book.amount_size &&
                setCartProducts([
                  ...cartProducts.slice(0, cartProducts.indexOf(book)),
                  ...cartProducts.slice(cartProducts.indexOf(book) + 1),
                ]),
              book.amount < book.amount_size && book.amount++,
              book.amount < book.amount_size && console.log(book)
            )}
          />
        )
        }
        <Buy cartProducts={cartProducts} />
      </div>
    </div>
  );
}

const mapDispatchToProps = (dispatch: Dispatch) =>({
  getBooksAction:bindActionCreators(getBooks,dispatch),
})
const mapStateToProps = (state: any) => ({
  books: state.booksReducers.books
})

export default connect(mapStateToProps,mapDispatchToProps)(PageTemplate);
