export type Book = {
  id: number;
  name: string;
  image: string;
  price: number;
  amount: number;
  amount_size: number;
};
