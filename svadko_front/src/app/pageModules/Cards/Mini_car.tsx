import Icons from "../CRUD_icons/Icons";
import Minus from "../CRUD_icons/Minus";
import styles from "../pageThemes/main.module.css";
import { Book } from "../Class/Book";
import Money_bar from "../CRUD_icons/Money_bar";

type PropsCartProducts = {
  cartProducts: Book[];
};

export function Num({ cartProducts }: PropsCartProducts) {
  let summa = 0;
  cartProducts.map((book) => {
    summa += book.price;
  });
  return (
    <div className={styles.Num}>
      <h4>{summa}</h4>
    </div>
  );
}

export default function Mini_card({ cartProducts }: PropsCartProducts) {
  return (
    <div>
      <Money_bar></Money_bar>
      <Num cartProducts={cartProducts}></Num>
      {cartProducts.map((book, index) => (
        <div key={index} className={styles.Mini_card}>
          <h4>{book.name}</h4>
          {/* <Icons>
            <Num cartProducts={cartProducts}></Num>
          </Icons> */}
        </div>
      ))}
    </div>
  );
}
