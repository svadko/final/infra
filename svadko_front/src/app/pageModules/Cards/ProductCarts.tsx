import Card from "./Card";
import styles from "../pageThemes/main.module.css";
import Minus from "../CRUD_icons/Minus";
import Plus from "../CRUD_icons/Plus";
import Icons from "../CRUD_icons/Icons";
import { useState, useEffect } from "react";
import Money_case from "../CRUD_icons/Money_case";
import { Book } from "../Class/Book";

type ProductCardProps = {
  books: Book[];
  addProduct: (value: Book) => void;
  minusProduct: (value: Book) => void;
};
export default function ProductCards({
  books,
  addProduct,
  minusProduct,
}: ProductCardProps) {
  return (
    <div className={styles.OuterCards}>
      {books.map((book, index) => (
        <Card
          key={index}
          book={book}
          addProduct={addProduct}
          minusProduct={minusProduct}
        />
      ))}
    </div>
  );
}
