import Minus from "../CRUD_icons/Minus";
import Plus from "../CRUD_icons/Plus";
import Icons from "../CRUD_icons/Icons";
import styles from "../pageThemes/main.module.css";
import { useState, useEffect, SetStateAction, Dispatch } from "react";
import Money_case from "../CRUD_icons/Money_case";
import addProduct from "../PageTemplate";
import { Book } from "../Class/Book";
import Image from "next/image";
// import { Image } from "next/image";
type Card = {
  book: Book;
  addProduct: (value: Book) => void;
  minusProduct: (value: Book) => void;
};
export default function Card({ book, addProduct, minusProduct }: Card) {
  return (
    <div className={styles.Card}>
      <Image
        className={styles.image}
        alt={book.name}
        src={book.image}
        width={100 * 1.3}
        height={150 * 1.3}
      ></Image>
      {/* <img src={book.image}></img> */}
      <h1 className={styles.Card_name}>{book.name}</h1>
      {/* <h1>{book.amount}</h1> */}
      <Icons>
        <div className={styles.Num}>
          <h3>{book.amount}</h3>
        </div>
        <div onClick={() => addProduct(book)}>
          <Plus />
        </div>
        <div onClick={() => minusProduct(book)}>
          <Minus />
        </div>

        <Money_case money={book.price} />
        {/* <Money_case money={book.amount} /> */}
        <h4 className={styles.case_money}>{book.price}</h4>
      </Icons>
    </div>
  );
}
