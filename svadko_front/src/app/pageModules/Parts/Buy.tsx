import { title } from "process";
import styles from "../pageThemes/main.module.css";
import Icon_1 from "../Icons/Icon_1";
import Icon_2 from "../Icons/Icon_2";
import Link from "next/link";
import Buy_icon from "../CRUD_icons/Buy_icon";
import Money_bar from "../CRUD_icons/Money_bar";
import Mini_card from "../Cards/Mini_car";
import {Book} from "../Class/Book";


const pages = [
  { title: "Новинки", path: "/page/new/1", select: "/new" },
  { title: "Популярное", path: "/page/pop/1", select: "/pop" },
  { title: "По автору", path: "/page/au/1", select: "/au" },
  { title: "Бренды", path: "/page/br/1", select: "/br" },
];
type PropsCartProducts = {
  cartProducts: Book[];
}
export default function Buy({cartProducts}: PropsCartProducts) {
  return (
    <div>
      <div className={styles.InnerLeft}>
        <Buy_icon/>
        {/* <Money_bar/> */}
        <Mini_card cartProducts={cartProducts}/>
      </div>
    </div>
  );
}
