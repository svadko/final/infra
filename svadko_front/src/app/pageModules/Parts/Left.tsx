import { title } from "process";
import styles from "../pageThemes/main.module.css";
import Icon_1 from "../Icons/Icon_1";
import Icon_2 from "../Icons/Icon_2";
import Link from "next/link";

const pages = [
  { title: "Новинки", path: "/page/new/1", select: "/new" },
  { title: "Популярное", path: "/page/pop/1", select: "/pop" },
  { title: "По автору", path: "/page/au/1", select: "/au" },
  { title: "Бренды", path: "/page/br/1", select: "/br" },
];

export default function Left() {
  return (
    <div>
      <div className={styles.InnerLeft}>
        <h3>Виды продуктов</h3>
        {pages.map(({ title, path, select }, index) => (
          <Link key={index} href={path}>
            <p>{title}</p>
          </Link>
        ))}
      </div>
    </div>
  );
}
