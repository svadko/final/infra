import * as types from "./types";
import { Book } from "@/app/pageModules/Class/Book";

export function getBooks(data: Book[]) {
    return {data,type:types.GET_BOOKS};
}