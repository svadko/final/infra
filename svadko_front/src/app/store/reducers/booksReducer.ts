import * as types from '../actions/types';
import { Book } from '@/app/pageModules/Class/Book';

interface CompaniesState {
    isLoading: boolean;
    books: Book[];
  }

const initialState: CompaniesState = {
    isLoading: false,
    books: [],
};

export default function booksReducers(state=initialState, action: any) {
    switch(action.type) {
        case types.SUCCESS_GET_BOOKS: 
            return {...state,books:action.payload.data};
        default:
            return state;
    }
}