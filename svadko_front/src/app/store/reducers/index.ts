import {combineReducers} from 'redux';
import booksReducers from './booksReducer';

export default combineReducers({
    booksReducers
})