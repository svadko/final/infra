import {all , put , takeLatest} from 'redux-saga/effects';
import * as types from '../actions/types';
import axios from 'axios'

function* getBooks(): Generator<any, void, any>{
    try {
        const BOOKS = yield axios.get(`http://104.248.32.114:8080/books`);
        yield put({
                type: types.SUCCESS_GET_BOOKS,
                payload: BOOKS
            })
    } catch(e) {
        yield put({type:types.FAILURE_GET_BOOKS, errors:e})
    }
}

export function* booksSagas(): Generator{
    yield all([
        yield takeLatest(types.GET_BOOKS, getBooks)
    ])
}