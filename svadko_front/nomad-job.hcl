variable "docker_image" {}

job "svadko_job" {
  datacenters = ["dc1"]

  type = "service"

  group "svadko_front" {
    count = 1

    network {
      port "svadko_front" {
        to = 3000
      }
    }

    
    task "svadko_task" {
      driver = "docker"

      config {
        image = var.docker_image
        network_mode = "host"
        ports = ["svadko_front"]
      }
    } 
  }
}
