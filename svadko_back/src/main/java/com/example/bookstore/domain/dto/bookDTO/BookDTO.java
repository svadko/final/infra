package com.example.bookstore.domain.dto.bookDTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class BookDTO {
    private Long id;
    @JsonProperty("name")
    private String title;
    private Double price;
    private String description;

    @JsonProperty("image")
    private String imageUrl;

    private Integer amount;

    @JsonProperty("amount_size")
    private Integer amountSize;

}
