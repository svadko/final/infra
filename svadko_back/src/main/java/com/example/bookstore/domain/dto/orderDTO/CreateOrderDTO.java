package com.example.bookstore.domain.dto.orderDTO;

import com.example.bookstore.domain.dto.orderDetailDTO.CreateOrderDetailDTO;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateOrderDTO {
    private Long id;
    @JsonProperty("order_details")
    private List<CreateOrderDetailDTO> orderDetail;

}
