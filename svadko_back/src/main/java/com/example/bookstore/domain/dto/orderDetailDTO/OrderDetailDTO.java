package com.example.bookstore.domain.dto.orderDetailDTO;

import com.example.bookstore.domain.dto.bookDTO.BookDTO;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderDetailDTO {
    @JsonIgnore
    private Long id;
    private BookDTO book;
    private Integer amount;
}
