package com.example.bookstore.domain.dto.orderDetailDTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateOrderDetailDTO {
    @JsonProperty("book_id")
    private Long bookId;
    private Integer amount;
}
