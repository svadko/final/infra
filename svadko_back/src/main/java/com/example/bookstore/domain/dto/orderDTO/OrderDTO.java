package com.example.bookstore.domain.dto.orderDTO;

import com.example.bookstore.domain.dto.orderDetailDTO.OrderDetailDTO;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderDTO {
    private Long id;

    @JsonProperty("total_price")
    private Double totalPrice;
    @JsonProperty("order_details")
    private List<OrderDetailDTO> orderDetail;

}
