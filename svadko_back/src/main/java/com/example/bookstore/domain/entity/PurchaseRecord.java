package com.example.bookstore.domain.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.time.LocalDateTime;

@Entity
@Table(name = "purchase_records")
@Data
public class PurchaseRecord {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "timestamp")
    private LocalDateTime timestamp;

    @Column(name = "number_of_purchases")
    private int numberOfPurchases;

    @Column(name = "total_amount_spent")
    private Double totalAmountSpent;
}
