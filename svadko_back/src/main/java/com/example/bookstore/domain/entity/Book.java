package com.example.bookstore.domain.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Entity
@Table(name = "books")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "book_title")
    private String title;

    @Column(name = "book_price")
    private Double price;

    @Column(name = "book_description", columnDefinition = "text")
    private String description;

    @Lob
    @Column(name = "image", length = 1000)
    private byte[] image;

    @Column(name = "amount")
    private Integer amount;

    @Column(name = "amount_size")
    private Integer amountSize;

    @OneToMany(mappedBy = "book", cascade = CascadeType.REMOVE)
    private List<OrderDetail> orderDetailList;
}
