package com.example.bookstore.mapper;

import com.example.bookstore.domain.dto.bookDTO.BookDTO;
import com.example.bookstore.domain.entity.Book;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class BookMapper implements Mapper<BookDTO, Book>{
    @Value("${app.base-url}")
    private String baseUrl;

    @Override
    public Book mapToEntity(BookDTO bookDTO) {
//        Book book = new Book();
//        book.setId(bookDTO.getId());
//        book.setTitle(bookDTO.getTitle());
//        book.setDescription(bookDTO.getDescription());
//        book.setAmount(bookDTO.getAmount());
        return null;
    }

    @Override
    public BookDTO mapToDTO(Book book) {
        BookDTO bookDTO = new BookDTO();
        Long id = book.getId();
        bookDTO.setId(id);
        bookDTO.setTitle(book.getTitle());
        bookDTO.setPrice(book.getPrice());
        bookDTO.setDescription(book.getDescription());
        bookDTO.setAmount(book.getAmount());
        bookDTO.setAmountSize(book.getAmountSize());
        bookDTO.setImageUrl(baseUrl + "/books/" + id + "/image");
        return bookDTO;
    }
}
