package com.example.bookstore.mapper;

import com.example.bookstore.domain.dto.bookDTO.BookDTO;
import com.example.bookstore.domain.dto.orderDetailDTO.OrderDetailDTO;
import com.example.bookstore.domain.entity.OrderDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OrderDetailMapper implements Mapper<OrderDetailDTO, OrderDetail>{

    @Autowired
    private BookMapper bookMapper;

    @Override
    public OrderDetail mapToEntity(OrderDetailDTO orderDetailDTO) {
        return null;
    }

    @Override
    public OrderDetailDTO mapToDTO(OrderDetail orderDetail) {
        OrderDetailDTO orderDetailDTO = new OrderDetailDTO();
        orderDetailDTO.setId(orderDetail.getId());
        orderDetailDTO.setAmount(orderDetail.getAmount());

        BookDTO bookDTO = new BookDTO();
        orderDetailDTO.setBook(bookMapper.mapToDTO(orderDetail.getBook()));
        return orderDetailDTO;
    }
}
