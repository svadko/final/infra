package com.example.bookstore.mapper;

public interface Mapper<D, E>{
    E mapToEntity(D dto);
    D mapToDTO(E entity);
}
