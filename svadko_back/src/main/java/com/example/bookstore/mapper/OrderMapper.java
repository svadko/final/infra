package com.example.bookstore.mapper;

import com.example.bookstore.domain.dto.orderDTO.OrderDTO;
import com.example.bookstore.domain.dto.orderDetailDTO.OrderDetailDTO;
import com.example.bookstore.domain.entity.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class OrderMapper implements Mapper<OrderDTO, Order>{

    @Autowired
    private OrderDetailMapper orderDetailMapper;

    @Override
    public Order mapToEntity(OrderDTO dto) {
        return null;
    }

    @Override
    public OrderDTO mapToDTO(Order order) {
        OrderDTO orderDTO = new OrderDTO();

        orderDTO.setId(order.getId());
        orderDTO.setTotalPrice(order.getTotalPrice());

        List<OrderDetailDTO> orderDetailDTO = order.getOrderDetailList()
                .stream()
                .map(orderDetail -> orderDetailMapper.mapToDTO(orderDetail))
                .collect(Collectors.toList());

        orderDTO.setOrderDetail(orderDetailDTO);
        return orderDTO;
    }
}
