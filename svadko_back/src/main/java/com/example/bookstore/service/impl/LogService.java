package com.example.bookstore.service.impl;

import com.example.bookstore.domain.entity.Log;
import com.example.bookstore.repository.LogRepository;
import com.opencsv.CSVWriter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.StringWriter;
import java.util.List;

@Service
@RequiredArgsConstructor
public class LogService {
    final private LogRepository logRepository;

    public byte[] getFile(){
        List<Log> dataList = logRepository.findAll();
        StringWriter writer = new StringWriter();
        CSVWriter csvWriter = new CSVWriter(writer);
        csvWriter.writeNext(new String[]{"id","message","time"});
        for (Log log : dataList) {
            csvWriter.writeNext(new String[]{log.getId() + "", log.getMessage(), log.getTime() + ""});
        }

        // Prepare the CSV content as bytes
        byte[] csvBytes = writer.toString().getBytes();
        return csvBytes;
    }

}
