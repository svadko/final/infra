package com.example.bookstore.service;

import com.example.bookstore.domain.dto.Status;
import com.example.bookstore.domain.dto.orderDTO.CreateOrderDTO;
import com.example.bookstore.domain.dto.orderDTO.OrderDTO;

import java.util.List;

public interface OrderService {
    Status makeOrder(CreateOrderDTO createOrderDTO);

    List<OrderDTO> getOrders();
}
