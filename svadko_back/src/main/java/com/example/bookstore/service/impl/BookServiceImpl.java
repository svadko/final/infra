package com.example.bookstore.service.impl;

import com.example.bookstore.domain.dto.bookDTO.BookDTO;
import com.example.bookstore.domain.entity.Book;
import com.example.bookstore.mapper.BookMapper;
import com.example.bookstore.mapper.Mapper;
import com.example.bookstore.repository.BookRepository;
import com.example.bookstore.service.BookService;
import com.example.bookstore.utils.ImageUtils;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {
    final private BookRepository bookRepository;

    final private ModelMapper modelMapper;

    final private BookMapper bookMapper;

    @Value("${app.base-url}")
    private String baseUrl;

    @Override
    public BookDTO createBook(String title, Double price, MultipartFile image, String description, Integer amount, Integer amountSize) throws IOException {
        Book book = new Book();
        book.setTitle(title);
        book.setPrice(price);
        book.setImage(ImageUtils.compressImage(image.getBytes()));
        book.setDescription(description);
        book.setAmount(amount);
        book.setAmountSize(amountSize);
        book = bookRepository.save(book);
        return bookMapper.mapToDTO(book);
    }

    @Override
    public BookDTO getBook(long id) {
        Book book = bookRepository.findById(id).get();
        return bookMapper.mapToDTO(book);
    }

    @Override
    public List<BookDTO> getAllBooks() {
        List<Book> books = bookRepository.findAll();
        List<BookDTO> response = books.stream()
                .map(book -> bookMapper.mapToDTO(book))
                .collect(Collectors.toList());
        return response;
    }

    @Override
    public byte[] getImageOfBook(long id) {
        Book book = bookRepository.findById(id).get();
        byte[] image = ImageUtils.decompressImage(book.getImage());
        return image;
    }

}
