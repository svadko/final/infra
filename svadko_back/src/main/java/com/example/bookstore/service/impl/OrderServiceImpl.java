package com.example.bookstore.service.impl;

import com.example.bookstore.domain.dto.Status;
import com.example.bookstore.domain.dto.orderDTO.CreateOrderDTO;
import com.example.bookstore.domain.dto.orderDTO.OrderDTO;
import com.example.bookstore.domain.dto.orderDetailDTO.OrderDetailDTO;
import com.example.bookstore.domain.entity.Book;
import com.example.bookstore.domain.entity.Order;
import com.example.bookstore.domain.entity.OrderDetail;
import com.example.bookstore.domain.entity.PurchaseRecord;
import com.example.bookstore.mapper.OrderMapper;
import com.example.bookstore.repository.BookRepository;
import com.example.bookstore.repository.OrderDetailRepository;
import com.example.bookstore.repository.OrderRepository;
import com.example.bookstore.repository.PurchaseRecordRepository;
import com.example.bookstore.service.BookService;
import com.example.bookstore.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Transactional
public class OrderServiceImpl implements OrderService {
    private final OrderRepository orderRepository;
    private final OrderDetailRepository orderDetailRepository;
    private final BookRepository bookRepository;

    private final PurchaseRecordRepository purchaseRecordRepository;

    private final OrderMapper orderMapper;

    @Transactional(rollbackFor = {IllegalStateException.class})
    @Override
    public Status makeOrder(CreateOrderDTO createOrderDTO) {
        Order order = new Order();
        List<Double> prices = new LinkedList<>();

        List<OrderDetail> orderDetailList = createOrderDTO.getOrderDetail()
                .stream()
                .map(createOrderDetailDTO -> {
                    OrderDetail orderDetail = new OrderDetail();
                    Book book = bookRepository.findById(createOrderDetailDTO.getBookId()).get();

                    if (book == null || book.getAmount() < createOrderDetailDTO.getAmount()) {
                        throw new IllegalStateException("Insufficient stock or invalid book.");
                    }
                    Integer amount = createOrderDetailDTO.getAmount();
                    book.setAmount(book.getAmount() - amount);
                    book.setAmountSize(book.getAmountSize() - amount);

                    if (book.getAmount() < 0 || book.getAmountSize() < 0) {
                        // Check if the amount becomes negative
                        throw new IllegalStateException("Negative stock detected.");
                    }

                    bookRepository.save(book);
                    orderDetail.setBook(book);
                    orderDetail.setAmount(amount);
                    orderDetail.setOrder(order);
                    prices.add(amount * book.getPrice());
                    return orderDetailRepository.save(orderDetail);
                })
                .collect(Collectors.toList());

        double totalPrice = prices.stream().mapToDouble(Double::doubleValue)
                .sum();

        order.setTotalPrice(totalPrice);
        order.setOrderDetailList(orderDetailList);
        order.setPurchasedAt(LocalDateTime.now());
        orderRepository.save(order);
        return new Status("ok");
    }

    @Override
    public List<OrderDTO> getOrders() {
        List<OrderDTO> orders = orderRepository.findAll()
                .stream().map(order -> orderMapper.mapToDTO(order))
                .collect(Collectors.toList());
        return orders;
    }

    @Scheduled(fixedRate = 60000) // Run every minute
    public void performETL() {
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime oneMinuteAgo = now.minusMinutes(1);

        List<Order> ordersInLastMinute = orderRepository.findOrdersInLastMinute(oneMinuteAgo, now);
        int numberOfPurchases = ordersInLastMinute.size();
        double totalAmountSpent = ordersInLastMinute.stream()
                .mapToDouble(order -> order.getTotalPrice())
                .sum();

        PurchaseRecord purchaseRecord = new PurchaseRecord();
        purchaseRecord.setTimestamp(now);
        purchaseRecord.setNumberOfPurchases(numberOfPurchases);
        purchaseRecord.setTotalAmountSpent(totalAmountSpent);

        purchaseRecordRepository.save(purchaseRecord);
    }

}
