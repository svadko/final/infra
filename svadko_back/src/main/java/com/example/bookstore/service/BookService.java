package com.example.bookstore.service;

import com.example.bookstore.domain.dto.bookDTO.BookDTO;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface BookService {
    BookDTO createBook(String title, Double price, MultipartFile image, String description, Integer amount, Integer amountSize) throws IOException;
    BookDTO getBook(long id);
    byte[] getImageOfBook(long id);
//    BookDTO updateBook(long id, BookDTO bookDTO); to do
    List<BookDTO> getAllBooks();
//    void deleteBook(long id); to do

}
