package com.example.bookstore.controller;

import com.example.bookstore.domain.dto.Status;
import com.example.bookstore.domain.dto.orderDTO.CreateOrderDTO;
import com.example.bookstore.domain.dto.orderDTO.OrderDTO;
import com.example.bookstore.service.impl.OrderServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/order")
@CrossOrigin
@RequiredArgsConstructor
public class OrderController {
    private final OrderServiceImpl orderService;

    @PostMapping("/make")
    private ResponseEntity makeOrder(@RequestBody CreateOrderDTO createOrderDTO) {
        Status status = orderService.makeOrder(createOrderDTO);
        return new ResponseEntity(status, HttpStatus.OK);
    }

    @GetMapping ("/list")
    private ResponseEntity getOrders() {
        List<OrderDTO> orders = orderService.getOrders();
        return new ResponseEntity(orders, HttpStatus.OK);
    }
}
