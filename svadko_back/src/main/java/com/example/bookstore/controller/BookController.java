package com.example.bookstore.controller;

import com.example.bookstore.domain.dto.bookDTO.BookDTO;
import com.example.bookstore.service.impl.BookServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/books")
@CrossOrigin
@RequiredArgsConstructor
public class BookController {
    private final BookServiceImpl bookService;

    @GetMapping()
    private ResponseEntity getBook() {
        try {
            List<BookDTO> response = bookService.getAllBooks();
            return new ResponseEntity(response, HttpStatus.OK);
        } catch (Exception e) {
            Map<String, String> errorResponse = new HashMap<>();
            errorResponse.put("error", e.getMessage());
            return new ResponseEntity(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE,
    produces = MediaType.APPLICATION_JSON_VALUE)
    private ResponseEntity createBook(@RequestParam("title") String title,
                                      @RequestParam("price") Double price,
                                      @RequestParam("image") MultipartFile image,
                                      @RequestParam("description") String description,
                                      @RequestParam("amount") Integer amount,
                                      @RequestParam("amount_size") Integer amountSize
    ) {
        try {
            BookDTO response = bookService.createBook(title, price, image, description, amount, amountSize);
            return new ResponseEntity(response, HttpStatus.OK);
        } catch (Exception e) {
            Map<String, String> errorResponse = new HashMap<>();
            errorResponse.put("error", e.getMessage());
            return new ResponseEntity(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{id}")
    private ResponseEntity getBook(@PathVariable long id) {
        try {
            BookDTO response = bookService.getBook(id);
            return new ResponseEntity(response, HttpStatus.OK);
        } catch (Exception e) {
            Map<String, String> errorResponse = new HashMap<>();
            errorResponse.put("error", e.getMessage());
            return new ResponseEntity(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @GetMapping("/{id}/image")
    private ResponseEntity getImage(@PathVariable long id) {
        try {
            byte[] response = bookService.getImageOfBook(id);
            return ResponseEntity.status(HttpStatus.OK)
                    .contentType(MediaType.valueOf("image/png"))
                    .body(response);
        } catch (Exception e) {
            Map<String, String> errorResponse = new HashMap<>();
            errorResponse.put("error", e.getMessage());
            return new ResponseEntity(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
