package com.example.bookstore.controller;

import com.example.bookstore.service.impl.LogService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.swing.text.html.parser.Entity;

@RestController
@RequestMapping("/logs")
@CrossOrigin
@RequiredArgsConstructor
public class LogController {
    final private LogService logService;

    @GetMapping("/download")
    public ResponseEntity downloadFile() {
        byte[] file = logService.getFile();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("text/csv"));
        headers.setContentDispositionFormData("attachment", "data.csv");
        return new ResponseEntity<>(file, headers, HttpStatus.OK);
    }
}
