package com.example.bookstore.repository;

import com.example.bookstore.domain.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface OrderRepository extends JpaRepository<Order, Long> {

    @Query("SELECT o FROM Order o WHERE o.id BETWEEN :startId AND :endId")
    List<Order> findOrdersInLastMinute(@Param("startId") Long startId, @Param("endId") Long endId);

    @Query("SELECT o FROM Order o WHERE o.purchasedAt BETWEEN :startTime AND :endTime")
    List<Order> findOrdersInLastMinute(@Param("startTime") LocalDateTime startTime, @Param("endTime") LocalDateTime endTime);
}
